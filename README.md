# Automatic batching of work based on Promises

Sometimes work can be done quicker in batches (e.g. inserting multiple rows into a database in one transaction, or making a request to a remote server that supports batch requests). This package helps to automatically collect individual calls (items of work) into batches.

While one batch is busy being flushed, another batch is prepared in background to be flushed next. Error handling resizes batches to find and prune any items that cause errors.

Order of items is always preserved.

## Usage

```sh
yarn add autobatch-promise
```

```js
const Autobatch = require('autobatch-promise');
const batch = new Autobatch(flushCallback, {onError, maxBatchSize, maxQueueSize})
…
batch.add("item1");
batch.add("item2");
batch.add("item3");
```

## `new Autobatch(cb, options)`

 * `flushCallback(batchArray)` — function receiving an array if items to flush. If it throws an error when the batch contains more than one item, the items will be flushed again in a smaller batch.
 * options:
     * `onError(error, item)` — callback called whenever the `flushCallback` failed to flush the item.
     * `maxBatchSize` — maximum number of items to flush in one go. 0 (the default) means no limit.
     * `maxQueueSize` — maximum number of items stored and waiting to be flushed. When there are more items than this limit, some items will be dropped. 0 (the default) means no limit (other than Node running out of memory).

## `autobatch.add(item, [ item… ])`

Queues all arguments as items to be flushed. The items can be any JS value. They will be passed as-is to the `flushCallback`.

Returns a `Promise` which will resolve when all items are flushed. It never returns errors.

## `autobatch.flush()`

Waits until all items are flushed. It never returns errors. It's not necessary to call this function, since `add()` flushes as well.
