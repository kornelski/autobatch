module.exports = class Autobatch {
    constructor(flushCallback, {onError, maxBatchSize, maxQueueSize} = {}) {
        if ('function' !== typeof flushCallback) {
            throw Error("Missing callback arg");
        }
        this._flushCallback = flushCallback;
        this._errorCallback = onError;
        this._openQueue = [];
        this._flushInProgress = null;
        this._currentMaxBatchSize = 0; // 0 = _maxBatchSize limit
        this._maxBatchSize = maxBatchSize || 0; // 0 = no limit
        this._maxQueueSize = maxQueueSize;
    }

    add(...items) {
        if (this._maxQueueSize && this._openQueue.length + items.length > this._maxQueueSize) {
            // Trim queue by 5% to avoid exponential cost when adding 1 item when it's full
            this._openQueue.splice(0, Math.ceil(this._openQueue.length/20 + items.length));
        }
        this._openQueue.push(...items);
        // It never fails, because Promises aren't created per-item, so they wouldn't report the right error.
        return this.flush();
    }

    flush() {
        if (!this._flushInProgress) {
            this._flushInProgress = this._flush();
        }
        return this._flushInProgress;
    }

    async _flush() {
        try {
            while(this._openQueue.length) {
                // Remove a batch from the queue. The queue may grow while flush is in progress.
                const queueInProgress = this._openQueue.splice(0, this._currentMaxBatchSize || this._maxBatchSize || this._openQueue.length);

                try {
                    await this._flushCallback(queueInProgress);
                    this._currentMaxBatchSize = 0; // On success return to normal-size batches
                } catch(err) {
                    // If this item causes failure, throw it away
                    if (queueInProgress.length == 1) {
                        if (this._errorCallback) {
                            try {this._errorCallback(err, queueInProgress[0]);} catch(_) {}
                        }
                        continue;
                    }

                    // Restore items for the next try. If the queue is full, then just drop them.
                    if (!this._maxQueueSize || queueInProgress.length + this._openQueue.length < this._maxQueueSize) {
                        this._openQueue = queueInProgress.concat(this._openQueue);
                    }
                    // On error reduce batch size to narrow it down to the one item that is failing
                    this._currentMaxBatchSize = Math.ceil(queueInProgress.length / 3);
                }
            }
        } finally {
            this._flushInProgress = null;
        }
    }
}
