const Autobatch = require('..');
const assert = require('assert');

describe('Autobatch', function(){
    it('simple flush', async function(){
        let lastFlush;
        const a = new Autobatch(function(batch){
            lastFlush = batch;
            return;
        });
        await a.add('foo');
        assert.deepEqual(lastFlush, ['foo']);
        await a.add('bar');
        assert.deepEqual(lastFlush, ['bar']);
    });

    it('simple immediate flush', async function(){
        let lastFlush;
        const a = new Autobatch(function(batch){
            lastFlush = batch;
            return;
        });
        a.add('foo');
        assert.deepEqual(lastFlush, ['foo']);
        await a.add('bar');
        assert.deepEqual(lastFlush, ['bar']);
    });

    it('queued flush', async function(){
        let lastFlush;
        let resolve;
        let called = 0;
        const a = new Autobatch(function(batch){
            lastFlush = batch;
            called++;
            return new Promise(r => {
                resolve = r;
            });
        });
        a.add('foo');
        assert.deepEqual(lastFlush, ['foo']);
        assert.equal(1, called);

        a.add('bar');
        resolve();
        assert.equal(1, called);
        const bazPromise = a.add('baz');
        // Processing is async, so wait until it's processed
        await new Promise(r => process.nextTick(r));
        assert.equal(2, called);
        assert.deepEqual(lastFlush, ['bar', 'baz']);
        resolve();
        await bazPromise;
    });

    it('queued error', async function(){
        let flushed = [];
        let flushes = 0;
        let errors = 0;
        const a = new Autobatch(function(batch){
            if (batch.some(x => x === 'err')) {
                throw Error("Nope!");
            }
            flushes++;
            flushed.push(...batch);
        }, {onError(err, item) {
            if (item === 'err') errors++; else errors = 666;
        }});
        a.add(1,2,3,'err',4);
        await a.add(5,6,7);
        await a.add(8,'err',9);
        assert.deepEqual(flushed, [1,2,3,4,5,6,7,8,9]);
        assert.equal(flushes, 6);
        assert.equal(errors, 2);
    });

    it('queue limit', async function(){
        let flushed = 0;
        const a = new Autobatch(function(batch){
            flushed += batch.length;
            return Promise.resolve();
        }, {maxQueueSize: 10});

        for(let i=0; i < 100; i++) {
            assert(a._openQueue.length <= 10);
            a.add(1,2,3,4,5);
        }
        await a.flush();
        assert(flushed < 50, flushed);
    });

    it('batch limit', async function(){
        let maxSize = 0;
        const a = new Autobatch(function(batch){
            maxSize = Math.max(maxSize, batch.length);
            return Promise.resolve();
        }, {maxBatchSize: 2});

        for(let i=0; i < 100; i++) {
            a.add(1,2,3,4,5);
        }
        await a.flush();
        assert(maxSize, 2);
    });
})
